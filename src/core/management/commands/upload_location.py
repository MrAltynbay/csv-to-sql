import csv
import os

from core.models import Country, Region, City
from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Display all directories of csv files'

    def handle(self, *args, **kwargs):
        base_dir = os.path.dirname(settings.BASE_DIR)
        countries_dir = os.path.join(base_dir, 'data', '_countries.csv')
        cities_dir = os.path.join(base_dir, 'data', '_cities.csv')
        regions_dir = os.path.join(base_dir, 'data', '_regions.csv')
        if not os.path.isfile(cities_dir):
            self.stdout.write(f'_cities.csv not in this dir: {cities_dir}')
            return
        if not os.path.isfile(countries_dir):
            self.stdout.write(f'_countries.csv not in this dir: {countries_dir}')
            return
        if not os.path.isfile(regions_dir):
            self.stdout.write(f'_regions_dir.csv not in this dir: {regions_dir}')
            return
        # Загрузка _countries.csv в бд
        with open(countries_dir, "r", encoding='utf-8') as countries_file:
            reader = csv.DictReader(countries_file)
            countries = []
            for number, line in enumerate(reader):
                country = Country(id=line['id'], title_ru=line['title_ru'],
                                  title_en=line['title_en'], title_es=line['title_es'])
                countries.append(country)
                if number % 10000 == 0:
                    Country.objects.bulk_create(countries)
                    countries.clear()
            if countries:
                Country.objects.bulk_create(countries)

        # Загрузка _regions.csv в бд
        with open(regions_dir, "r", encoding='utf-8') as regions_file:
            reader = csv.DictReader(regions_file)
            regions = []
            for number, line in enumerate(reader):
                region = Region(id=line['id'], title_ru=line['title_ru'],
                                title_en=line['title_en'], title_es=line['title_es'], country_id=line['country'])
                regions.append(region)
                if number % 10000 == 0:
                    Region.objects.bulk_create(regions)
                    regions.clear()
            if regions:
                Region.objects.bulk_create(regions)

        # Загрузка _cities.csv в бд
        with open(cities_dir, "r", encoding='utf-8') as cities_file:
            reader = csv.DictReader(cities_file)
            cities = []
            for number, line in enumerate(reader):

                city = City(id=line['city_id'], title_ru=line['title_ru'],
                            title_en=line['title_en'], title_es=line['title_es'],
                            area_ru=line['area_ru'], area_en=line['area_en'], area_es=line['area_es'],
                            region_id=line['region_id'],
                            country_id=line['country_id'])
                cities.append(city)
                if number % 10000 == 0:
                    City.objects.bulk_create(cities)
                    cities.clear()
            if cities:
                City.objects.bulk_create(cities)
