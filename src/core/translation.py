from modeltranslation.translator import TranslationOptions, translator

from core.models import Country, Region, City


class CountryTranslationOptions(TranslationOptions):
    fields = ("title",)


translator.register(Country, CountryTranslationOptions)


class RegionTranslationOptions(TranslationOptions):
    fields = ("title",)


translator.register(Region, RegionTranslationOptions)


class CityTranslationOptions(TranslationOptions):
    fields = (
        "title",
        "area",
    )


translator.register(City, CityTranslationOptions)
